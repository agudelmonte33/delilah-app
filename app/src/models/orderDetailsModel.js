const {Model, DataTypes} = require('sequelize');
const sequelize = require('../connection/sequelize');

class OrderDetails extends Model {}

OrderDetails.init({
    quantity: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {sequelize, modelName: "order_details"});

module.exports = {OrderDetails}