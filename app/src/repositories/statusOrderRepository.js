const {
    StatusOrder
} = require('../models/statusOrderModel');

async function addStatusOrder(status) {
    const newStatusOrder = await StatusOrder.create({
        status: status,
    });
    return newStatusOrder.toJSON();
};

async function getStatusOrders() {
    try {
        const statusOrders = await StatusOrder.findAll();
        return statusOrders;
    } catch (error) {
        console.log(error);
    }
};

async function getStatusOrderById(StatusOrderId) {
    const StatusOrderById = await StatusOrder.findOne({
        where: {
            id: StatusOrderId
        }
    })
    return StatusOrderById;
};

async function updateStatusOrder(statusOrderId, status) {
    return await StatusOrder.update({
        status: status
    }, {
        where: {
            id: statusOrderId
        }
    })
};

async function enableStatusOrder(statusOrderId) {
    await StatusOrder.update({
        enable: false
    }, {
        where: {
            id: statusOrderId
        }
    })
};

module.exports = {
    addStatusOrder,
    getStatusOrders,
    getStatusOrderById,
    updateStatusOrder,
    enableStatusOrder
}