const mercadopago = require("mercadopago");
const config = require('../config/config');

const createPreference = (req, res) => {
    let order = req.body.order;
	let items = [];
	order.forEach(element => {
		let item = {
			title: element.product.name,
			unit_price: Number(element.product.price),
			quantity: Number(element.quantity)
		}
		items.push(item);
	});

	let preference = {
		items: items,
		back_urls: {
			"success": `${config.server_host}/checkout/mercadopago/feedback`,
			"failure": `${config.server_host}/checkout/mercadopago/failed`,
			"pending": `${config.server_host}/checkout/mercadopago/feedback`
		},
		auto_return: "approved",
	};

	mercadopago.preferences.create(preference)
		.then(function (response) {
			res.json({
				id: response.body.id
			});
		}).catch(function (error) {
			console.log(error);
		});
}

const feedback = (req, res) => {
    res.redirect(`${config.client_host}/client/compra-exitosa.html`)
}

module.exports = {createPreference, feedback};
