const config = require('../config/config');
const axios = require('axios');


const createOrder = async (req, res) => {
    const {order, total} = req.body;
    let items = [];
    order.forEach(item => {
        items.push({ name: item.product.name, quantity: item.quantity, unit_price: item.product.price });
    })
    
    try {
        const orders = {
            intent: "CAPTURE",
            purchase_units: [{
                amount: {
                    currency_code: "USD",
                    value: total,
                },
                
            }, ],
            //TODO: detallar el listado de item
            application_context: {
                brand_name: "Delilah Restó",
                landing_page: "NO_PREFERENCE",
                user_action: "PAY_NOW",
                return_url: `${config.server_host}/checkout/paypal/capture-order`,
                cancel_url: `${config.server_host}/checkout/paypal/cancel-payment`,
            },
        };
        const params = new URLSearchParams();
        params.append('grant_type', 'client_credentials')

        const {
            data: {
                access_token
            }
        } = await axios.post('https://api-m.sandbox.paypal.com/v1/oauth2/token', params, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            auth: {
                username: config.paypal_client_id,
                password: config.paypal_client_secret
            }
        })

        const response = await axios.post(`${config.paypal_sandbox_url}/v2/checkout/orders`, orders, {
            headers: {
                Authorization: `Bearer ${access_token}`,
            }
        })
        console.log(response.data.links);
        res.json(response.data.links);
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Error en el servidor');
    }
};

const captureOrder = async (req, res) => {

    const {
        token
    } = req.query;
    try {
        const response = await axios.post(
            `${config.paypal_sandbox_url}/v2/checkout/orders/${token}/capture`, {}, {
                auth: {
                    username: config.paypal_client_id,
                    password: config.paypal_client_secret,
                },
            }
        );

        console.log({data: response.data});
        res.redirect(`${config.client_host}/client/compra-exitosa.html`);
    } catch (error) {
        console.log(error.message);
        return res.status(500).json({
            message: "Internal Server error"
        });
    }
};

const cancelPayment = (req, res) => {
    res.send('Payment canceled');
};

module.exports = {createOrder, captureOrder, cancelPayment}
