const jwt = require('jsonwebtoken');
const {tokenGenerator} = require('../helper/tokenGeneratorHelper');
const {findOrCreateUser} = require('../helper/findOrCreateUserHelper');

async function userVerify(req, res, next) {
    if (req.isAuthenticated()) {
        try {
            const user = await findOrCreateUser(req.user);
            const token = tokenGenerator(user);
            req.headers.authorization = `Bearer ${token}`;
        } catch (error) {
            console.log(error);
        }
    } 

    try {
        const {secret_key} = require('../config/config');
        const bearerToken = req.headers.authorization;
        const token = bearerToken.split(" ")[1];
        const decoded = jwt.verify(token, secret_key);
        req.payLoad = decoded;
        next();
    } catch (error) {
        console.error({mensaje: 'Token inválido', error});
        res.status(409).json({ status: 'No tienes autorización'});
    }
}

module.exports = {userVerify}