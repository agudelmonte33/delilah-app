const { updateOrderDetails } = require("../repositories/orderDetailsRepository");

async function updaterDetails(details, orderId) {
    try {
        for await (let detail of details) {
            await updateOrderDetails(orderId, detail.quantity, detail.productId);
        }
    } catch (error) {
        console.log(error);
    }
    return true;
}

module.exports = { updaterDetails }