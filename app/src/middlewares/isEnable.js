const jwt = require('jsonwebtoken');

function isEnable(req, res, next) {
    const {secret_key} = require('../config/config');
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split(" ")[1];
    const decoded = jwt.verify(token, secret_key);
    if (decoded.enable === false) {
        res.status(401).json({message: "Estas suspendido"})
    } else {
        next();
    }
}

module.exports = {isEnable}