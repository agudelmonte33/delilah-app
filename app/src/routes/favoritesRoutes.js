const express = require('express');
const router = express.Router();
const {userVerify} = require('../middlewares/userVerify');
const {favoriteValid} = require('../middlewares/favoriteValid');
const { getFavoriteById, getFavorites, addFavorite, deleteFavorite } = require('../repositories/favoriteRepository');

router.post('/', userVerify, favoriteValid, async (req, res) => {
    const userId = req.user_id
    const {productId} = req.body
    try {
        const newFavorite = await addFavorite(userId, productId);
        res.status(201).json({message: "Producto agregado a favoritos", newFavorite: newFavorite});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.get('/', userVerify, async (req, res) => {
    const userId = req.user_id;
    try {
        const response = await getFavorites(userId);
        response != null ? res.status(200).json({response}) : res.status(400).json({error: "No tiene favoritos"});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.get('/:favoriteId', userVerify, async (req, res) => {
    const {favoriteId} = req.params;
    const userId = req.user_id;
    try {
        const getFavorite = await getFavoriteById(favoriteId, userId);
        getFavorite != null ? res.status(200).json(getFavorite) : res.status(404).json({error: "No existe este favorito en la base de datos"});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.delete('/:favoriteId', userVerify, async (req, res) => {
    const userId = req.user_id;
    const {favoriteId} = req.params;
    try {
        const response = await deleteFavorite(favoriteId, userId);
        console.log(response);
        response === 1 ? res.status(200).json({message: "Favorito eliminado"}) : res.status(409).json({error: "No se puso eliminar de favoritos"});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

module.exports = router