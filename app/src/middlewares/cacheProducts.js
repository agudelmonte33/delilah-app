const client = require("../connection/redis");

function cacheProducts(req, res, next) {
    client.get("products", (err, data) => {
        if (err) throw err;
        req.dataCache = JSON.parse(data);
        next();
    })
}

function cacheProductById(req, res, next) {
    client.get("product", (err, data) => {
        if (err) throw err;
        req.dataCache = JSON.parse(data);
        next();
    })
}

module.exports = {cacheProducts, cacheProductById}