const {
    Product
} = require('../models/productModel');

async function addProduct(name, shortName, price, description, img) {
    const newProduct = await Product.create({
        name: name,
        short_name: shortName,
        price: price,
        enable: true,
        description: description,
        img: img
    });
    return newProduct.toJSON();
};

async function getProducts() {
    const products = await Product.findAll({
        where: {
            enable: true
        }
    })
    return products;
};

async function getProductById(productId) {
    try {
        const productById = await Product.findOne({
            where: {
                id: productId
            }
        })
        return productById.toJSON();
    } catch (error) {
        console.log(error.message);
    }
};

async function updateProduct(productId, name, short_name, price, details, img) {
    return await Product.update({
        name: name,
        short_name: short_name,
        price: price,
        details: details,
        img: img
    }, {
        where: {
            id: productId
        }
    })
};

async function updatePriceProduct(price, productId) {
    return await Product.update({
        price: price
    },
    {
        where: {
            name: "Bagel de salmón"
        }
    }
    )
};

async function enableProduct(productId, enable) {
    return await Product.update({
        enable: enable,
    }, { 
        where: {
            id: productId
        }
    })
};

module.exports = {addProduct,
    getProducts,
    getProductById,
    updateProduct,
    enableProduct,
    updatePriceProduct
}