const mercadopago = new MercadoPago('TEST-a5613a50-2f44-49b6-a3e5-0bce58df0928', {
    locale: 'es-AR' // The most common are: 'pt-BR', 'es-AR' and 'en-US'
});

$("#payment-form").hide();

const ENV = "development";

const productsURL = ENV === "production" ? "https://delilah-resto.cf/api/products" : "http://localhost:3000/products";
const mercadopagoURL = ENV === "production" ? "https://delilah-resto.cf/api/checkout/mercadopago/create-preference" : "http://localhost:3000/checkout/mercadopago/create-preference";
const paypalURL = ENV === "production" ? "https://delilah-resto.cf/api/checkout/paypal/create-order" : "http://localhost:3000/checkout/paypal/create-order";

let myStorage = window.localStorage;

let carrito = [];
let productos;
let payLoad;
let order = [];
const divisa = '$';
const DOMitems = document.querySelector('#items');
const DOMcarrito = document.querySelector('#carrito');
const DOMtotal = document.querySelector('#total');
const DOMbotonVaciar = document.querySelector('#boton-vaciar');
const DOMcheckout = document.querySelector('#payment-form');
const DOMlistCheckout = document.querySelector('#list-group-flush');

//Me gerera una lista de productos dinámicamente 
function renderizarProductos() {
    let token = myStorage.getItem("token");
    fetch(productsURL, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "authorization": "Bearer " + token
            }
        })
        .then((r) => r.json().then((data) => ({
            status: r.status,
            body: data
        })))
        .then((info) => {
            if (info.status === 200 || info.status === 201) {
                productos = info.body;
                let html = '';
                productos.products.forEach(producto => {
                    html += `
                    <div class="col-md-6">
                        <div class="card mb-4 shadow-sm">
                            <img src="${producto.img}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">${producto.name}</p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline-secondary" onclick="agregarCarrito(${producto.id})">Añadir al carrito</button>
                                    </div>
                                    <small class="text-muted">${divisa}${producto.price}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    `
                });
                DOMitems.innerHTML = html;
            }
        })
        .catch(error => {
            alert(error)
        })
}

function agregarCarrito(id) {
    let producto = productos.products.find(producto => producto.id === id);
    carrito.push(producto);
    renderizarCarrito();
}

function renderizarCarrito() {
    let html = '';
    const carritoSinDuplicados = [...new Set(carrito)];
    carritoSinDuplicados.forEach(producto => {
        const cantidad = carrito.reduce((total, item) => {
            return item.id === producto.id ? total + 1 : total;
        }, 0);
        html += `
        <div class="row">
            <div class="col-md-12">
            <div class="d-flex justify-content-between align-items-center p-1">
                <button type="button" class="btn btn-danger btn-sm" onclick="borrarItemCarrito(${producto.id})">Eliminar</button>
                <p class="m-0">${cantidad} x ${producto.name} - ${divisa}${producto.price}</p>
            </div>
            </div>
        </div>
        `;
    });
    DOMcarrito.innerHTML = html;
    DOMtotal.innerHTML = `${divisa}${carrito.reduce((total, producto) => total + producto.price, 0)}`;
}

function borrarItemCarrito(id) {
    carrito = carrito.filter(producto => producto.id !== id);
    renderizarCarrito();
}

function vaciarCarrito() {
    carrito = [];
    renderizarCarrito();
}

// Create preference when click on checkout button
function createCheckoutButton(preferenceId) {
    // Initialize the checkout
    mercadopago.checkout({
        preference: {
            id: preferenceId
        },
        render: {
            container: '#button-checkout', // Class name where the payment button will be displayed
            label: 'MercadoPago', // Change the payment button text (opti
        }
    });
}

function createCheckoutButtonPaypal() {
    paypal.Buttons({
        style: {
            shape: 'rect',
            color: 'gold',
            layout: 'horizontal',
            label: 'paypal',
            height: 37.98,
            tagline: false
        }
    }).render('#paypal-button-container');
}

function goBack() {
    $("#payment-form").fadeOut(500);
    setTimeout(() => {
        $('#button-checkout').empty();
        $(".shopping-cart").show(500).fadeIn();
    }, 500);
    $('#checkout-btn').attr("disabled", false);
}

function checkout() {

    //Crea el listado de productos del checkout
    let carritoSinDuplicados = [...new Set(carrito)];
    carritoSinDuplicados.forEach(producto => {
        const cantidad = carrito.reduce((total, item) => {
            return item.id === producto.id ? total + 1 : total;
        }, 0);
        order.push({
            product: producto,
            quantity: cantidad
        });
    })

    //Valor total de la compra
    let total = carrito.reduce((total, producto) => total + producto.price, 0);

    //checkout completo de los productos y su total
    payLoad = {
        order: order,
        total: total
    }

    //armado del html de checkout
    let htmlItemsCarrito = '';
    let htmlListChekout = '';
    carritoSinDuplicados.forEach(producto => {
        const cantidad = carrito.reduce((total, item) => {
            return item.id === producto.id ? total + 1 : total;
        }, 0);
        htmlItemsCarrito += `
                        <li class="list-group-item">
                            <div class="d-flex justify-content-between align-items-center p-1">
                                <div class="">${producto.name} x ${cantidad}</div>
                                <div class="" id="summary-price">$ ${producto.price}</div>
                            </div>
                        </li>
                        `
    });
    htmlListChekout = `
                    ${htmlItemsCarrito}
                    <li class="list-group-item">
                        <div class="d-flex justify-content-between align-items-center p-1">
                            <div><b>TOTAL</b></div>
                            <div class="" id="summary-price">$ ${total}</div>
                        </div>                     
                        `
    DOMlistCheckout.innerHTML = htmlListChekout;

    //Le pega a la API para pago con MercadoPago
    fetch(mercadopagoURL, {
            method: "POST",
            body: JSON.stringify(payLoad),
            headers: {
                "Content-Type": "application/json"
            },
        })
        .then(function (response) {
            return response.json();
        })
        .then(function (preference) {
            createCheckoutButton(preference.id);

            $(".shopping-cart").fadeOut(500);
            setTimeout(() => {
                $(".payment-form").show(500).fadeIn();
            }, 500);
        })
        .catch(function (e) {
            alert(`Unexpected error`);
            $('#checkout-btn').attr("disabled", false);
        });
}

 //Le pega a la API para pago con Paypal
function checkoutPaypal(){
    //Crea el listado de productos del checkout
    let carritoSinDuplicados = [...new Set(carrito)];
    carritoSinDuplicados.forEach(producto => {
        const cantidad = carrito.reduce((total, item) => {
            return item.id === producto.id ? total + 1 : total;
        }, 0);
        order.push({
            product: producto,
            quantity: cantidad
        });
    })

    //Valor total de la compra
    let total = carrito.reduce((total, producto) => total + producto.price, 0);

    //checkout completo de los productos y su total
    payLoad = {
        order: order,
        total: total
    }

    fetch(paypalURL, {
        method: "POST",
        body: JSON.stringify(payLoad),
        headers: {
            "Content-Type": "application/json"
        },
    })
    .then((r) => r.json().then((data) => ({
        status: r.status,
        body: data
    })))
    .then((info) => {
        if(info.status === 200){
            console.log(info.body);
            window.location.href = info.body[1].href;
        }
    })
    .catch(function (e) {
        alert(`Unexpected error`);
        $('#checkout-btn').attr("disabled", false);
    });
}

renderizarProductos();
renderizarCarrito();