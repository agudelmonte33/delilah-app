const jwt = require('jsonwebtoken');
const { secret_key } = require('../config/config');

function tokenGenerator(data) {
    try {
        const token = jwt.sign( {
            first_name: data.first_name,
            last_name: data.last_name,
            is_admin: data.is_admin,
            email: data.email,
            enable: data.enable,
            } , secret_key);
        return token;
    } catch (error) {
        console.log(error);
    }
}

module.exports = {tokenGenerator};