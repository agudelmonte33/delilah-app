const express = require('express');
const app = express();
const config = require('./src/config/config');
const sequelize = require('./src/connection/sequelize');
require('./src/models/models');
const helmet = require('helmet');
const cors = require('cors');
const passport = require('passport');
const session = require('express-session');
const { userVerify } = require('./src/middlewares/userVerify');
const { isLoggedIn } = require('./src/middlewares/isLoggedIn');
const morgan = require('morgan');

app.use(session({
    secret: config.app_session,
    resave: true,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(cors());
app.use(helmet());
app.use(morgan('dev'));

(async () => {
    try {
        await sequelize.sync();
    } catch (error) {
        console.log(error);
    }
})();

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

const users = require('./src/routes/usersRoutes');
app.use('/users', users);

const orders = require('./src/routes/ordersRoutes');
app.use('/orders', orders);

const products = require('./src/routes/productsRoutes');
app.use('/products', products);

const paymentMethods = require('./src/routes/paymentMethodsRoutes');
app.use('/payments', paymentMethods);

const addresses = require('./src/routes/addressRoutes');
app.use('/addresses', addresses);

const favorites = require('./src/routes/favoritesRoutes');
app.use('/favorites', favorites);

const checkout = require('./src/routes/checkoutRoutes');
app.use('/checkout', checkout);

const statusOrders = require('./src/routes/statusOrdersRoutes');
app.use('/statusOrders', statusOrders);

const authRoute = require('./src/routes/authRoutes');

app.use('/auth', authRoute);

app.use('/home', userVerify, (req, res) => {
    res.status(200).send('HA INICIADO SESIÓN CON ÉXITO');
})

app.listen(config.node_port, console.log(`Escuchando en el puerto ${config.node_port}`))

module.exports = app
