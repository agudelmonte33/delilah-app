const {
    getProductById
} = require("../repositories/productRepository");

async function amountCalculator(details) {
    let totalAmount = 0;
    for await (let detail of details) {
        const product = await getProductById(detail.productId);
        const productQuantity = parseInt(detail.quantity);
        totalAmount += (product.price * productQuantity);
    }
    return totalAmount;
}


module.exports = {amountCalculator}