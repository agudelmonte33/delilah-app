const {Favorite} = require('../models/favoriteModel')
const {Product} = require('../models/productModel')

async function addFavorite(userId, productId) {
    const newFavorite = await Favorite.create({
        userId: userId,
        productId: productId
    });
    return newFavorite; 
};

async function getFavorites(userId){
    const favorites = await Favorite.findAll({
        where:{
            userId: userId
        },
        include: {
            model: Product
        }
    })
    return favorites;
};

async function getFavoriteById(favoriteId, userId){
    try {
        const favorite = await Favorite.findOne({
            where: {
                id: favoriteId,
                userId: userId
            }
        })
        return favorite;
    } catch (error) {
        return error;
    }
};

async function getFavoriteByProductId(productId, userId){
    try {
        const favorite = await Favorite.findOne({
            where: {
                productId: productId,
                userId: userId
            }
        })
        return favorite;
    } catch (error) {
        console.log(error);;
    }
};



async function deleteFavorite(favoriteId) {
    return await Favorite.destroy({
        where: {
            id: favoriteId
        }
    })
};

module.exports = {addFavorite, getFavorites, getFavoriteById, getFavoriteByProductId, deleteFavorite}