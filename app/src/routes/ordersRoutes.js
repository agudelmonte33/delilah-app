const express = require('express');
const {
    amountCalculator
} = require('../helper/amountCalculator');
const {
    updaterDetails
} = require('../helper/detailsUpdater');
const {
    orderBuilder
} = require('../helper/orderBuilder');
const {
    orderDetailsBuilder
} = require('../helper/orderDetailsBuilder');
const {
    inputOrderValid
} = require('../middlewares/inputOrderValid');
const {
    isEnable
} = require('../middlewares/isEnable');
const {
    userVerify
} = require('../middlewares/userVerify');
const {
    getOrderById,
    updateOrder, getUserOrders, getOrders
} = require('../repositories/orderRepository');
const {
    isClose
} = require('../middlewares/isClose');
const {
    deleteOrderDetails
} = require('../repositories/orderDetailsRepository');
const router = express.Router();

router.post('/', userVerify, isEnable, inputOrderValid, async (req, res) => {
    const {
        details,
        payment,
        addressId
    } = req.body;
    const {userId} = req;
    try {
        const total = await amountCalculator(details);
        const newOrder = await orderBuilder(total, addressId, userId, payment.methodId);
        const orderDetails = await orderDetailsBuilder(details, newOrder.id);
        res.status(201).json({
            mensajes: "¡Recibimos tu pedido!",
            pedido: newOrder,
            detalle: orderDetails
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.get('/', userVerify, isEnable, async (req, res) => {
    const {
        userId,
        is_admin
    } = req.body;
    try {
        if (is_admin === true) {
            const allOrders = await getOrders();
            res.status(200).json({
            allOrders
        });
        } else {
            const userOrders = await getUserOrders(userId);
            res.status(200).json({
                userOrders
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.get('/:orderId', userVerify, isEnable, async (req, res) => {
    const {
        orderId
    } = req.params;
    try {
        const order = await getOrderById(orderId);
        res.status(200).json({
            order
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.put('/:orderId', userVerify, isEnable, isClose, async (req, res) => {
    let {
        orderId
    } = req.params;
    orderId = parseInt(orderId);
    const details = req.body;
    try {
        await updaterDetails(details, orderId);
        const order = await getOrderById(orderId);
        const total = await amountCalculator(order.order_details);
        const updatedOrder = await updateOrder(orderId, total, order.addressId, order.payment_methodId);
        res.status(200).json({
            message: "Order actualizada",
            detalle: updatedOrder
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.patch('/:orderId', userVerify, isEnable, isClose, async (req, res) => {
    const {
        orderId
    } = req.params;
    const details = req.body;
    try {
        await orderDetailsBuilder(details, orderId);
        const order = await getOrderById(orderId);
        const total = await amountCalculator(order.order_details);
        const updatedOrder = await updateOrder(orderId, total, order.addressId, order.payment_methodId);
        res.status(200).json({
            message: "Producto agregado",
            detalle: updatedOrder
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.delete('/:orderId/:orderDetailsId', userVerify, isEnable, isClose, async (req, res) => {
    let {
        orderId,
        orderDetailsId
    } = req.params;
    orderId = parseInt(orderId);
    orderDetailsId = parseInt(orderDetailsId);
    try {
        await deleteOrderDetails(orderDetailsId);
        const order = await getOrderById(orderId);    
        const total = await amountCalculator(order.order_details);
        const updatedOrder = await updateOrder(orderId, total, order.addressId, order.payment_methodId);
        res.status(200).json({
            message: "Producto eliminado",
            detalle: updatedOrder
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});



module.exports = router