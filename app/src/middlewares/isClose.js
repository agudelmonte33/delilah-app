const { getOrderById } = require("../repositories/orderRepository");

async function isClose(req, res, next) {
    const {orderId} = req.params;
    const order = await getOrderById(orderId);
    if (order.status === 2) {   
        return res.status(409).json({
            message: "Order is closed"
        });
    }   else {
        next();
    }
}

module.exports = { isClose };