const chai = require('chai');
const expect = chai.expect;
const request = require('supertest');
const app = require('../index');
const deleteUserTest = require('../src/database/scriptTest');

let userId;

describe('#index.js', function () {
    describe('/users', function () {
        it('Devuelve "201" si el registro es exitoso', function (done) {
            const payload = {
                first_name: "prueba",
                last_name: "prueba",
                nickname: "prueba",
                password: "prueba",
                email: "prueba@prueba.com",
                phone: 12542100
            }
            request(app)
                .post('/users/')
                .send(payload)
                .end(function (err, res) {
                    userId = res.body.newUser?.id
                    expect(res.statusCode).to.equal(201);
                    done();
                })
        });
        it('Devuelve "422" si el usuario no ingresa campos requeridos', function (done) {
            const payload = {
                first_name: "prueba",
                last_name: "admin",
                nickname: "admin",
                password: "admin",
                email: "",
                phone: 12542100
            }
            request(app)
                .post('/users/')
                .send(payload)
                .end(function (err, res) {
                    userId = res.body.newUser?.id
                    expect(res.statusCode).to.equal(422);
                    done();
                })
        });
        after(function(){
            if (userId != null) {
                deleteUserTest(userId);
            }
        });
    });
});