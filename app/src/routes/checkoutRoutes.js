const express = require('express');
const router = express.Router();
const config = require('../config/config');
const mercadopago = require("mercadopago");
const paypal = require('paypal-rest-sdk');
const {createOrder, captureOrder, cancelPayment} = require('../controllers/paypal.controller')
const {createPreference, feedback} = require('../controllers/mercadopago.controller')

mercadopago.configure({
	access_token: config.access_token,
});

paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': config.client_id,
    'client_secret': config.client_secret
});


router.post('/mercadopago/create-preference', createPreference);

router.get('/mercadopago/feedback', feedback);

router.post('/paypal/create-order', createOrder);

router.get('/paypal/capture-order', captureOrder);

router.get('/paypal/cancel-payment', cancelPayment);



module.exports = router
