const {
    User
} = require('../models/userModel');

async function addUser(firstName, lastName, nickname, password, email, phone, isAdmin) {
    !isAdmin ? false : isAdmin;
    try {     
        const newUser = await User.create({
            first_name: firstName,
            last_name: lastName,
            nickname: nickname,
            is_admin: isAdmin,
            password: password,
            email: email,
            phone: phone,
            enable: true
        });
        return newUser.toJSON();
    } catch (error) {
        console.log(error);
    }
};

async function getUsers() {
    try {
        const users = await User.findAll();
        return users.toJSON();
    } catch (error) {
        console.log(error);
    }
};

async function getUserByEmail(emailUser) {
    const userByEmail = await User.findOne({
        where: {
            email: emailUser
        }
    })
    return userByEmail;
};

async function updateUser(userId, firstName, lastName, nickname, isAdmin, password, email, phone) {
    return await User.update({
        first_name: firstName,
        last_name: lastName,
        nickname: nickname,
        is_admin: isAdmin,
        password: password,
        email: email,
        phone: phone
    }, {
        where: {
            id: userId
        }
    })
};

async function enableUser(userId, enable) {
    return await User.update({
        enable: enable
    }, { 
        where: {
            id: userId
        }
    })
};


module.exports = {
    addUser,
    getUsers,
    getUserByEmail,
    updateUser,
    enableUser
}
