require('dotenv').config();
const config = {
    node_port: process.env.NODE_PORT || 3000,
    client_local_port: process.env.CLIENT_LOCAL_PORT,
    server_host:process.env.SERVER_HOST,
    client_host: process.env.CLIENT_HOST,
    app_session: process.env.APP_SESSION,
    db_port: process.env.DB_PORT || 3306,
    db_user: process.env.DB_USER || '',
    db_pass: process.env.DB_PASS || '',
    db_host: process.env.DB_HOST || '',
    db_name: process.env.DB_NAME || 'delilah',
    secret_key: process.env.SECRET_KEY ||  '',
    redis_host: process.env.REDIS_HOST || '',
    redis_port: process.env.REDIS_PORT || 6379,
    google_app: process.env.GOOGLE_APP || '',
    google_client_id: process.env.GOOGLE_CLIENT_ID || '',
    google_client_secret: process.env.GOOGLE_CLIENT_SECRET || '',
    google_callback_url: process.env.GOOGLE_CALLBACK_URL || '',
    facebook_client_id: process.env.FACEBOOK_CLIENT_ID || '',
    facebook_client_secret: process.env.FACEBOOK_CLIENT_SECRET || '',
    facebook_callback_url: process.env.FACEBOOK_CALLBACK_URL || '',
    linkedin_client_id: process.env.LINKEDIN_CLIENT_ID || '',
    linkedin_client_secret: process.env.LINKEDIN_CLIENT_SECRET || '',
    linkedin_callback_url: process.env.LINKEDIN_CALLBACK_URL || '',
    access_token: process.env.ACCESS_TOKEN || 'access_token',
    paypal_client_id: process.env.PAYPAL_CLIENT_ID || '',
    paypal_client_secret: process.env.PAYPAL_CLIENT_SECRET || '',
    paypal_sandbox_url: process.env.PAYPAL_SANDBOX_URL||''
};

module.exports = config
