const {Order} = require('./orderModel')
const {User} = require('./userModel')
const {Product} = require('./productModel')
const {OrderDetails} = require('./orderDetailsModel')
const {PaymentMethod} = require('./paymentMethodModel')
const {StatusOrder} = require('./statusOrderModel')
const {Favorite} = require('./favoriteModel')
const {Address} = require('./addressModel')

Product.hasMany(OrderDetails, {as: "order_details", foreignKey: "productId"});
Product.hasMany(Favorite, {as: "favorites", foreignKey: "productId"});
Address.hasMany(Order, {as: "orders", foreignKey: "addressId"});
Order.hasMany(OrderDetails, {as: "order_details", foreignKey: "orderId"});
PaymentMethod.hasMany(Order, {as: "orders", foreignKey: "payment_methodId"});
StatusOrder.hasMany(Order, {as: "orders", foreignKey: "status_orderId"});
User.hasMany(Order, {as: "orders", foreignKey: "userId"});
User.hasMany(Address, {as: "addresses", foreignKey: "userId"});
User.hasMany(Favorite, {as: "favorites", foreignKey: "userId"});
Favorite.belongsTo(Product);

module.exports = {Order, User, Product, OrderDetails, PaymentMethod, StatusOrder, Favorite, Address}




























