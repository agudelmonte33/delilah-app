const {PaymentMethod} = require('../models/paymentMethodModel');

async function addPaymentMethod(paymentMethod) {
    const newPaymentMethod = await PaymentMethod.create({
        method: paymentMethod,
        enable: true
    });
    return newPaymentMethod.toJSON(); 
};

async function getPaymentMethod(){
    const paymentMethod = await PaymentMethod.findAll()
    console.log(paymentMethod);
    return paymentMethod;
};

async function getPaymentMethodById(paymentMethodId){
    const paymentMethodById = await PaymentMethod.findOne({
        where: {
            id: paymentMethodId
        }
    })
    return paymentMethodById;
};

async function updatePaymentMethod(paymentMethodId, paymentMethod, enable){
    return await PaymentMethod.update(
        {
            method: paymentMethod,
            enable: enable
        },
        {
            where: {
                id: paymentMethodId
            }
        }
        )
};

async function deletePaymentMethod(paymentMethodId) {
    return await PaymentMethod.destroy({
        where: {
            id: paymentMethodId
        }
    })
};

module.exports = {addPaymentMethod, getPaymentMethod, getPaymentMethodById, updatePaymentMethod, deletePaymentMethod}