module.exports = {
  apps : [{
    name: "delilah",
    script: './index.js',
    watch: true,
    env_local: {
        "NODE_ENV": "local",
        "API_DESCRIPTION": "Estas ejecutando tu API en modo desarrollo"
    },
    env_production: {
        "NODE_ENV": "production",
        "API_DESCRIPTION": "Estas ejecutando tu API en producción"
    }
  }],
};

