const {Model, DataTypes} = require('sequelize');
const sequelize = require('../connection/sequelize');

class PaymentMethod extends Model {}

PaymentMethod.init({
    method: {
        type: DataTypes.STRING,
        allowNull: false
    },
    enable: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
    }
}, {sequelize, modelName: "payment_methods"});


module.exports = {PaymentMethod}