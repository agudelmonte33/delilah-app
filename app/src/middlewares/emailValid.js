const { getUserByEmail } = require("../repositories/userRepository");

async function emailValid(req, res, next) {
    try {
        const {email} = req.body;
        const findEmail = await getUserByEmail(email);
        if (findEmail !== null) {
            res.status(422).json({message: "Email existente"})
        }else{
            next();
        }
    } catch (error) {
        res.status(500).json({ status: 'Error interno' })
    }
    return;
}

module.exports = emailValid