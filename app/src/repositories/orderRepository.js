const {Order} = require('../models/orderModel');
const {OrderDetails} = require('../models/orderDetailsModel');

async function addOrder(total, addressId, userId, payment_methodId, status_orderId) {
    const newOrder = await Order.create({
        total: total,
        addressId: addressId,
        userId: userId,
        payment_methodId: payment_methodId,
        status_orderId: status_orderId
    });
    return newOrder; 
};

async function getUserOrders(userId){
    const order = await Order.findAll({
        where:{
            userId: userId
        },
        include: {
            model: OrderDetails,
            as: "order_details"
        }
    })
    return order;
};

async function getOrderById(orderId){
    const orderById = await Order.findOne({
        where: {
            id: orderId
        },
        include: {
            model: OrderDetails,
            as: "order_details",
            attributes: ["productId", "quantity"]
        }
    })
    return orderById;
};

async function getOrders(){
    const allOrders = await Order.findAll();
    return allOrders;
};

async function updateOrder(orderId, total, addressId, payment_methodId){
    return await Order.update(
        {
            total: total,
            addressId: addressId,
            payment_methodId: payment_methodId

        },
        {
            where: {
                id: orderId
            }
        }
        )
};

async function deleteOrder(orderId) {
    await Order.destroy({
        where: {
            id: orderId
        }
    })
};

module.exports = {addOrder, getUserOrders, getOrderById, getOrders, updateOrder, deleteOrder};