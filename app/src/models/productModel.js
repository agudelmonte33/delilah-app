const {Model, DataTypes} = require('sequelize');
const sequelize = require('../connection/sequelize');
const {Favorite} = require('./favoriteModel');
const {OrderDetails} = require('./orderDetailsModel');

class Product extends Model {}

Product.init({
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    short_name: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    price: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    enable: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
    },
    description:{
        type: DataTypes.STRING,
        allowNull: false
    },
    img: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {sequelize, modelName: "products"});


module.exports = {Product}