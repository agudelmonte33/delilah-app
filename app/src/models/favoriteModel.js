const {Model, DataTypes} = require('sequelize');
const sequelize = require('../connection/sequelize');

class Favorite extends Model {}

Favorite.init({}, {sequelize, modelName: "favorites"});

module.exports = {Favorite}