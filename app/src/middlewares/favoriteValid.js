const { getFavoriteByProductId } = require("../repositories/favoriteRepository");

async function favoriteValid(req, res, next) {
    const {productId} = req.body;
    const userId = req.user_id
    try {
        const finded = await getFavoriteByProductId(productId, userId);
        console.log(finded);
        if (finded !== null) {
            res.status(422).json({message: "Este producto ya esta entre tus favoritos"})
        }else{
            next();
        }
    } catch (error) {
        res.status(500).json({ status: 'Error interno' })
    }
    return;
}

module.exports = {favoriteValid}