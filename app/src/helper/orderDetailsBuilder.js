const { addOrderDetails} = require('../repositories/orderDetailsRepository');

async function orderDetailsBuilder(details, orderId) {
    try {
        const orderDetails = [];
        for await (let detail of details) {
            await addOrderDetails(detail.quantity, orderId, detail.productId);
            orderDetails.push({product_id: detail.productId, quantity: detail.quantity});
        }
        return orderDetails;
    } catch (error) {
        console.log(error.message);
    }
}

module.exports = {orderDetailsBuilder};