const ENV = "development";

const loginURL = ENV === "production" ? "https://delilah-resto.cf/api/users/login" : "http://localhost:3000/users/login";
const registerURL = ENV === "production" ? "https://delilah-resto.cf/api/users/" : "http://localhost:3000/users";
const homeURL = ENV === "production" ? "https://delilah-resto.cf/api/home" : "http://localhost:3000/home";


let myStorage = window.localStorage;


function login(e) {
    e.preventDefault();

    const form = document.getElementById("form");
    const email = form.email.value;
    const password = form.password.value;

    if (!email || !password) {
        alert("Todos los campos son requeridos");
        return false;
    }
    const formData = {
        email: email,
        password: password
    };
    fetch(loginURL, {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            },
        })
        .then((r) => r.json().then((data) => ({
            status: r.status,
            body: data
        })))
        .then((info) => {
            if (info.status === 200 || info.status === 201) {
                console.log({
                    status: info.status,
                    body: info.body
                })
                let myStorage = window.localStorage;

                myStorage.setItem("token", info.body.token);
                let token = myStorage.getItem("token");

                fetch(homeURL, {
                        method: "GET",
                        headers: {
                            "Content-Type": "application/json",
                            "authorization": "Bearer " + token
                        }
                    })
                    .then((r) => {
                        if (info.status === 200 || info.status === 201) {
                            console.log("status: " + info.status)
                            window.location.href = homeURL;
                        } else {
                            alert(info.body.message)
                        }
                    })
                    .catch(error => {
                        alert("Error interno")
                    })
            }

        })
        .catch(error => {
            alert(error)
        })

}

function register(e) {
    e.preventDefault();

    const form = document.getElementById("form");

    const firstName = form.firstName.value;
    const lastName = form.lastName.value;
    const nickname = form.nickname.value;
    const email = form.email.value;
    const password = form.password.value;
    const phone = form.phone.value;
    const pass = form.pass.value;

    if (!firstName || !lastName || !nickname || !phone || !pass || !email) {
        alert("Todos los campos son requeridos");
        return false;
    }


    const formData = {
        first_name: firstName,
        last_name: lastName,
        nickname: nickname,
        email: email,
        phone: phone,
        password: password
    };

    fetch(registerURL, {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            },
        })
        .then((r) => r.json().then((data) => ({
            status: r.status,
            body: data
        })))
        .then((info) => {
            if (info.status === 200 || info.status === 201) {
                console.log({
                    status: info.status,
                    body: info.body
                })

                let myStorage = window.localStorage;

                myStorage.setItem("token", info.body.token);
                let token = myStorage.getItem("token");

                fetch(homeURL, {
                        method: "GET",
                        headers: {
                            "Content-Type": "application/json",
                            "authorization": "Bearer " + token
                        }
                    })
                    .then((r) => {
                        if (r.status === 200 || r.status === 201) {
                            console.log("status: " + r.status)
                            window.location.href = homeURL;
                        } else {
                            alert(r.body.message)
                        }
                    })
            }
        })
        .catch(error => {
            alert(error)
        })
}