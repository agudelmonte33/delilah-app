const express = require('express');
const router = express.Router();
const {userVerify} = require('../middlewares/userVerify');
const {isAdmin} = require('../middlewares/isAdmin');
const { getStatusOrderById, getStatusOrders, addStatusOrder, enableStatusOrder, updateStatusOrder } = require('../repositories/statusOrderRepository');

router.get('/', userVerify, isAdmin, async (req, res) => {
    try {
        const userId = req.user_id;
        console.log(userId);
        const getStatusOrder = await getStatusOrders();
        res.status(200).json({getStatusOrder});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.post('/', userVerify, isAdmin, async (req, res) => {
    try {
        const {status} = req.body
        const newStatusOrder = await addStatusOrder(status);
        res.status(201).json({message: "Nuevo Estado agregado", newStatusOrder});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.get('/:statusOrderId', userVerify, isAdmin, async (req, res) => {
    try {
        const {statusOrderId} = req.params;
        console.log(statusOrderId);
        const getStatusOrder = await getStatusOrderById(statusOrderId);
        res.status(200).json(getStatusOrder);
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.put('/:statusOrderId', userVerify, isAdmin, async (req, res) => {
    const {statusOrderId} = req.params;
    const {statusOrder} = req.body;
    try {
        const upStatusOrder = await updateStatusOrder(statusOrderId, statusOrder);
        res.status(201).json({upStatusOrder});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.delete('/:statusOrderId', userVerify, isAdmin, async (req, res) => {
    try {
        const {statusOrderId} = req.params;
        await enableStatusOrder(statusOrderId);
        res.status(200).json({message: "Estado eliminado"});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

module.exports = router