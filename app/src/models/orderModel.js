const {Model, DataTypes} = require('sequelize');
const sequelize = require('../connection/sequelize');

class Order extends Model {}

Order.init({
    total: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
}, {sequelize, modelName: "orders"});


module.exports = {Order}