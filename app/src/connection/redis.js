const { createClient } = require('redis');
const {redis_port, redis_host} = require('../config/config')
const client = createClient(redis_port, redis_host);

module.exports = client
