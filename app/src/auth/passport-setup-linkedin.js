require("dotenv").config();

const config = require('../config/config');
const passport = require('passport');
const LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

passport.use(new LinkedInStrategy({
        clientID: config.linkedin_client_id,
        clientSecret: config.linkedin_client_secret,
        callbackURL: config.linkedin_callback_url,
        scope: ['r_emailaddress', 'r_liteprofile'],
    },
    function (accessToken, refreshToken, profile, done) {
        const payLoad = {
            first_name: profile.name.givenName,
            last_name : profile.name.familyName,
            nickname: profile.displayName,
            enable: true,
            email: profile.emails[0].value,
            accessToken: accessToken
        }
        console.log({mensaje: 'Use LinkedinStrategy', payLoad});
        return done(null, payLoad);
    }
));