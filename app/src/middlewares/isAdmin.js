const jwt = require('jsonwebtoken');

function isAdmin(req, res, next) {
    const {secret_key} = require('../config/config');
    const bearerToken = req.headers.authorization;
    const token = bearerToken.split(" ")[1];
    try {
        const decoded = jwt.verify(token, secret_key);
        console.log(decoded);
        if (decoded.is_admin === true) {
            req.isAdmin = decoded.is_admin;
            next();
        } else {
            res.status(403).json({message: "No tiene autorización"});
        }
    } catch (error) {
        res.status(403).json({ status: 'Error interno' });
    }
}

module.exports = {isAdmin}