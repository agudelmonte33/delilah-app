require("dotenv").config();

const config = require('../config/config');
const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

passport.use(new FacebookStrategy({
        clientID: config.facebook_client_id,
        clientSecret: config.facebook_client_secret,
        callbackURL: config.facebook_callback_url,
        profileFields: ['id', 'displayName', 'photos', 'email', 'first_name']
    },
    function (accessToken, refreshToken, profile, done) {
        const payLoad = {
            first_name: profile.name.givenName,
            last_name : profile.name.familyName,
            nickname: profile.displayName,
            enable: true,
            email: profile._json.email,
            accessToken: accessToken
        }
        console.log({mensaje: 'Use FacebookStrategy', payLoad});
        return done(null, payLoad);
    }
));