function inputOrderValid(req, res, next) {
    const {details, payment, addressId} = req.body;
    if (!details || !payment || !addressId) {
        res.status(400).json({message: "Todos los campos son obligatorios"})
    } else {
        next();
    }
}

module.exports = {inputOrderValid}