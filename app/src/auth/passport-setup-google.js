const config = require('../config/config');
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

passport.use(new GoogleStrategy({
        clientID: config.google_client_id,
        clientSecret: config.google_client_secret,
        callbackURL: config.google_callback_url
    },
    function (accessToken, refreshToken, profile, done) {
        const payLoad = {
            first_name: profile._json.given_name,
            last_name : profile._json.family_name,
            nickname: profile._json.name,
            password: '',
            email: profile._json.email,
            phone: '',
            isAdmin: false,    
            }
        console.log({mensaje: 'Use GoogleStrategy', payLoad});
        return done(null, payLoad);
    }
));