const client = require("../connection/redis");

function cleanCache() {
    client.del("products");
    return true;
};

module.exports = {cleanCache}