const {Model, DataTypes} = require('sequelize');
const sequelize = require('../connection/sequelize');
const {Order} = require('./orderModel')

class StatusOrder extends Model {}

StatusOrder.init({
    status:{
        type: DataTypes.STRING,
        allowNull: false
    }
}, {sequelize, modelName: "status_order"});



module.exports = {StatusOrder}