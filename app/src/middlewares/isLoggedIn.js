module.exports = {
    isLoggedIn(req, res, next){
        try {
            const {secret_key} = require('../config/config');
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split(" ")[1];
            const decoded = jwt.verify(token, secret_key);
            req.payLoad = decoded;
            next();
        } catch (error) {
            console.error({mensaje: 'Token inválido', error});
            res.status(401).redirect('https://de6te8z0m4eih.cloudfront.net');
        }
        
    }
}