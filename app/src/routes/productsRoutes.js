const express = require('express');
const { isAdmin } = require('../middlewares/isAdmin');
const { userVerify } = require('../middlewares/userVerify');
const { cacheProducts, cacheProductById } = require('../middlewares/cacheProducts');
const {addProduct, getProducts, getProductById, updateProduct, enableProduct, updatePriceProduct} = require('../repositories/productRepository')
const router = express.Router();
const client = require('../connection/redis');
const { cleanCache } = require('../helper/cleanCache');

router.post('/', isAdmin, async (req, res) => {
    const {name, short_name, price, description, img} = req.body;
    try {
        const newProduct = await addProduct(name, short_name, price, description, img);
        res.status(201).json({newProduct});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' })
    }
});

router.get('/', cacheProducts, async (req, res) => {
    try {
        const dataCache = req.dataCache;
        if (dataCache !== null) {
            console.log('Datos obtenidos del cache');
            res.status(200).json({products: dataCache});
        } else {
            const products = await getProducts();
            client.set("products", JSON.stringify(products), 'EX', 60);
            console.log('Datos obtenidos de la base de datos');
            res.status(200).json({products});
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.get('/:productId', userVerify, cacheProductById, async (req, res) => {
    const { productId } = req.params;
    try {
        const dataCache = req.dataCache;
        if (dataCache != null && dataCache.id == productId) {
            console.log('Datos obtenidos del cache');
            res.status(200).json({productById : dataCache});
        } else {
            const productById = await getProductById(productId);
            client.set("product", JSON.stringify(productById), 'EX', 60);
            console.log('Datos obtenidos de la base de datos');
            res.status(200).json({productById});
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
})

router.patch('/:productId', isAdmin, async (req, res) => {
    const {productId} = req.params;
    const {enable} = req.body;
    try {
        const response = await enableProduct(productId, enable);
        response == 1 ? res.status(201).json({response}) : res.status(401).json({message: "Ha ocurrido un error"});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.put('/:productId', async (req, res) => {
    const {productId} = req.params;
    const {name, short_name, price, details, img} = req.body;
    try {
        const response = await updateProduct(productId, name, short_name, price, details, img);
	if (response[0] === 1) {
        res.status(201).json({response});
        } else {
            res.status(401).json({message: "Ha ocurrido un error al actualizar el producto"});
        }
    } catch (error) {
	    console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.patch('/:productId/price', isAdmin, async (req, res) => {
    const {productId} = req.params;
    const { price } = req.body;
    try {
        const response = await updatePriceProduct(price, productId);
        cleanCache();
        res.status(201).json({response});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});



module.exports = router
