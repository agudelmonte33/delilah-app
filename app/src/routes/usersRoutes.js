const express = require('express');
const router = express.Router();
const {addUser, getUserByEmail, enableUser} = require('../repositories/userRepository')
const emailValid = require('../middlewares/emailValid');
const {isAdmin} = require('../middlewares/isAdmin');
const {inputRegisterUserValid} = require('../middlewares/inputRegisterUserValid');
const { tokenGenerator } = require('../helper/tokenGeneratorHelper');


router.post('/', emailValid, inputRegisterUserValid, async (req, res) => {
    const {first_name, last_name, nickname, password, email, phone} = req.body;
    try {
        const newUser = await addUser(first_name, last_name, nickname, password, email, phone);
        const token = tokenGenerator(newUser);
        res.status(201).json({token});
    } catch (error) {
	    console.log(error);
        res.status(500).json({ status: 'Error interno' })
    }
});

router.post('/login', async (req, res) => {
    const {email, password} = req.body;
    try {
        const getUser = await getUserByEmail(email);
        const user = getUser.dataValues;
        if (password != '' && user.password === password) {
            const token = tokenGenerator(user);
            res.status(200).json({token});
        } else {
            res.status(403).send({message: "La contraseña es incorrecta"})
        }
    } catch (error) {
        console.log({error: error})
        res.status(500).json({ status: 'Error interno' })
    }    
});


router.patch('/:userId', isAdmin, async (req, res) => {
    const {userId} = req.params
    const {enable} = req.body;
    try {
        await enableUser(userId, enable);
        if (enable === false) {
            res.status(201).json({message: "Usuario deshabilitado"});
        } else {
            res.status(201).json({message: "Usuario habilitado"});
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
})

module.exports = router
