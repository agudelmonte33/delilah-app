const {getStatusOrders} = require('../repositories/statusOrderRepository');
const {addOrder} = require('../repositories/orderRepository');

async function orderBuilder(total, addressId, userId, paymentMethod) {
    try {
        const statusOrders = await getStatusOrders();
        const newStatusOrder = statusOrders.find(status => status.status = "nuevo");
        const newOrder = await addOrder(total, addressId, userId, paymentMethod, newStatusOrder.id);
        return newOrder;
    } catch (error) {
        console.log(error);
        return false
    }
}

module.exports = { orderBuilder } 