function inputRegisterUserValid(req, res, next) {
    const {first_name, last_name, nickname, password, email, phone} = req.body;
    if (!first_name || !last_name || !nickname || !password || !email || !phone) {
        res.status(400).json({message: "Todos los campos son obligatorios"})
    } else {
        next();
    }
}

module.exports = {inputRegisterUserValid}