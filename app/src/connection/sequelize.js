const {db_user, db_pass, db_host, db_name} = require('../config/config');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(db_name, db_user, db_pass, {
    host: db_host,
    dialect: 'mysql',
    logging: false,
    allowPublicKeyRetrieval: true
});


module.exports = sequelize;
