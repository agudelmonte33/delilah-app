const {getUserByEmail, addUser} = require('../repositories/userRepository');

async function findOrCreateUser(data){
    try {
        const user = await getUserByEmail(data.email);
        if(user){
            return user;
        } else {
            try {
                let user = await addUser(data.first_name, data.last_name, data.nickname, data.password, data.email, data.phone);
                return user;
            } catch (error) {
                console.log(error);
            }
        }
    } catch (error) {
        console.log(error);
    }
}

module.exports = {findOrCreateUser};