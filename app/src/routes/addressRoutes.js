const express = require('express');
const router = express.Router();
const {getAddressById, getAddress, addAddress, updateAddress, deleteAddress} = require('../repositories/addressRepository');
const {userVerify} = require('../middlewares/userVerify');
const {isEnable} = require('../middlewares/isEnable');

router.post('/', userVerify, isEnable, async (req, res) => {
    const userId = req.userId;
    const {address} = req.body
    try {
        const newAddress = await addAddress(address, userId);
        res.status(201).json({message: "Dirección agregada con éxito", newAddress: newAddress});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.get('/', userVerify, async (req, res) => {
    const userId = req.userId;
    try {
        const userAddresses = await getAddress(userId);
        userAddresses != null? res.status(200).json({userAddresses}) : res.status(404).json({error: "No hay direcciones para mostrar"});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.get('/:addressId', userVerify, async (req, res) => {
    const {addressId} = req.params;
    const userId = req.userId;
    try {
        const getAddress = await getAddressById(addressId, userId);
        getAddress != null ? res.status(200).json(getAddress) : res.status(404).json({error: "La dirección no existe"});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.put('/:addressId', userVerify, async (req, res) => {
    const {addressId} = req.params;
    const {address} = req.body;
    const userId = req.userId;
    try {
        const response = await updateAddress(addressId, address, userId);
        response[0] === 1 ? res.status(201).json({message: "Dirección modificada con éxito", response}) : res.status(403).json({error: "No se ha podido modificar la dirección", response});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.delete('/:addressId', userVerify, async (req, res) => {
    const {addressId} = req.params;
    const userId = req.userId;
    try {
        const response = await deleteAddress(addressId, userId);
        response === 1 ? res.status(201).json({message: "Dirección eliminada"}) : res.status(403).json({error: "No se pudo eliminar la dirección"});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});


module.exports = router
