const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const {addPaymentMethod, updatePaymentMethod, deletePaymentMethod, getPaymentMethodById, getPaymentMethod} = require('../repositories/paymentMethodRepository');
const {userVerify} = require('../middlewares/userVerify');
const { isAdmin } = require('../middlewares/isAdmin');

router.post('/', userVerify, isAdmin, async (req, res) => {
    const {payment_method} = req.body;
    try {
        const newPaymentMethod = await addPaymentMethod(payment_method);
        res.status(201).json({newPaymentMethod});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' })
    }
});

router.get('/', userVerify, async (req, res) => {
    try {
        const allMethods = await getPaymentMethod()
        res.status(200).json({allMethods});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
})

router.get('/:methodId', userVerify, async (req, res) => {
    const { methodId } = req.params
    try {
        const methodById = await getPaymentMethodById(methodId);
        res.status(200).json({methodById});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
})

router.put('/:methodId', userVerify, isAdmin, async (req, res) => {
    const {method, enable} = req.body;
    const { methodId } = req.params
    try {
        const updatePM = await updatePaymentMethod(methodId, method, enable);
        res.status(201).json({updatePM});
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

router.delete('/:methodId', userVerify, isAdmin, async (req, res) => {
    const {methodId} = req.params;
    try {
        const deleteMethod = await deletePaymentMethod(methodId);
        res.status(200).json({deleteMethod}); 
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 'Error interno' });
    }
});

module.exports = router