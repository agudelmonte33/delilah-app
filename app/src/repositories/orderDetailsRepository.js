const {OrderDetails} = require('../models/orderDetailsModel');

async function addOrderDetails(quantity, orderId, productId) {
    try { 
        const newOrderDetails = await OrderDetails.create({
            quantity: quantity,
            orderId: orderId,
            productId: productId
        });
        return newOrderDetails; 
    } catch (error) {
        console.log(error.message);
    }
};

async function getOrderDetails(userId){
    const orderDetails = await OrderDetails.findAll({
        where:{
            userId: userId
        }
    })
    return orderDetails;
};

async function getOrderDetailsById(orderDetailsId){
    const orderDetailsById = await OrderDetails.findOne({
        where: {
            id: orderDetailsId
        }
    })
    return orderDetailsById.toJSON();
};

async function updateOrderDetails(orderId, quantity, productId) {
    return await OrderDetails.update(
        {
            quantity: quantity
        },
        {
            where: {
                orderId: orderId,
                productId: productId
            }
        }
        )
};

async function deleteOrderDetails(orderDetailsId) {
    return await OrderDetails.destroy({
        where: {
            id: orderDetailsId
        }
    })
};

module.exports = {addOrderDetails, getOrderDetails, getOrderDetailsById, updateOrderDetails, deleteOrderDetails}