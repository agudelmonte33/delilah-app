# "Delilah Restó"

## Recursos y tecnologías utilizadas

- Node.js
- Nodemon
- Dotenv
- Redis
- JWT para autenticación
- Express
- Helmet
- Sequelize
- MySQL 
- Mocha
- Chai
- Postman para manejo de endpoints y testing
- Swagger para documentación de API

## Instalación e inicializacion del proyecto en entorno local

### 1 - Clonar proyecto

Clonar el repositorio desde el [siguiente link](https://gitlab.com/agudelmonte33/delilah-app).

Desde la consola con el siguiente link:

`git clone https://gitlab.com/agudelmonte33/delilah-app.git .`

### 2 - Tecnologías requeridas para el despliegue

1) Docker
2) Postman

### 3 - Definir las variables de entorno

el proyecto cuenta con un `.env.example` como referencia para definir las variables de entorno. 
### 4 - Inicializando Docker

- Modificar las variables de entorno del archivo `docker-compose.yml` para la conexión a a base de datos

```
 environment:
        - MYSQL_ROOT_PASSWORD: MYSQL_ROOT_PASSWORD
        - MYSQL_DATABASE: MYSQL_DATABASE #nombre de datos
```

- Ejecutar el siguiente comando para crear la red mediante la cual se comunicaran los contenedores: 

`docker network create delilah-network`

- Una vez creada la red, ejecutar el siguiente comando:

`docker-compose up`

¡y listo, ya tienes la app corriendo en local!

### 5 - Test

#### En entorno local

Para testear los endpoint, el proyecto con una `Delilah APP.postman_collection.json` o puedes ingresar en [este link](https://go.postman.co/workspace/Ac%C3%A1mica~a054e7c6-9f35-4b57-a9eb-cc1b62f66e12/collection/11359469-8d51676a-ca90-4233-9bb5-ce6b4f1191be?action=share&creator=11359469)

Para testear el loggeo con los diferentes Provedores de Identidad y las pasarelas de pago, se provee de un front en la directorio `/client`, que puedes correrlo utilizando a extensión *Live server* para VS Code. 

Para testear las diferentes pasarelas de pago (Mercadopago y Paypal) puedes hacer desde la interfas que se provee en `/client/tienda.html`

#### En entorno de producción

Para testear el loggeo con los diferentes Provedores de Identidad y las pasarelas de pago, se provee un front alojado en un bucket de AWS 

https://de6te8z0m4eih.cloudfront.net

