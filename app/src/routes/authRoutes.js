require("dotenv").config();
require('../auth/passport-setup-google');
require('../auth/passport-setup-facebook');
require('../auth/passport-setup-linkedin');

const express = require('express');
const router = express.Router();
const passport = require('passport');
const session = require('express-session');
const config = require('../config/config');

router.use(passport.initialize());
router.use(passport.session())

router.use(session({
    secret: config.app_session,
    resave: true,
    saveUninitialized: true
}));

//Auth Google
router.get('/google',
    passport.authenticate('google', {
        scope: ['profile', 'email']
    })
);

router.get('/google/callback', passport.authenticate('google', {
    failureRedirect: '/auth/failed',
    successRedirect: '/home'
}));


//Auth Facebook
router.get('/facebook',
    passport.authenticate('facebook')
);

router.get('/facebook/callback',
    passport.authenticate('facebook', {
        failureRedirect: '/auth/failed',
        successRedirect: '/home'
    }));

//Auth Linkedin
router.get('/linkedin',
    passport.authenticate('linkedin', {
        scope: ['r_liteprofile', 'r_emailaddress'],
        credentials: 'include'
    })
);

router.get('/linkedin/callback',
    passport.authenticate('linkedin', {
        failureRedirect: '/auth/failed',
        successRedirect: '/home',
    }),
);


//TODO: probarlo en el front
// Usada tanto para todos los passport
router.get('/logout', (req, res) => {
    req.logOut();
    console.log('logged out');
    res.status(200).send('logged out');
})

router.get('/failed', (req, res) => {
    res.status(401).json({
        "Mensaje": "Falla al loguearse"
    });
});
module.exports = router
