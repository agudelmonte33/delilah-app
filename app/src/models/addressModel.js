const {Model, DataTypes} = require('sequelize');
const sequelize = require('../connection/sequelize');
const {Order} = require('./orderModel');

class Address extends Model {}

Address.init({
    address:{
        type: DataTypes.STRING,
        allowNull: false
    }
}, {sequelize, modelName: "addresses"});



module.exports = {Address}