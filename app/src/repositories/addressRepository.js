const {Address} = require('../models/addressModel')

async function addAddress(address, userId) {
    const newAddress = await Address.create({
        address: address,
        userId: userId
    });
    return newAddress.toJSON(); 
};

async function getAddress(userId){
    const address = await Address.findAll({
        where:{
            userId: userId
        }
    })
    return address;
};

async function getAddressById(addressId, userId){
    try {
        const addressById = await Address.findOne({
        where: {
            id: addressId,
            userId: userId
            }
        })
        return addressById;
    } catch (error) {
        console.log(error.message);
    }
};

async function updateAddress(addressId, address, userId){
    return await Address.update(
        {
            address: address
        },
        {
            where: {
                id: addressId,
                userId: userId
            }
        }
        )
};

async function deleteAddress(addressId, userId) {
    return await Address.destroy({
        where: {
            id: addressId,
            userId: userId
        }
    })
};

module.exports = {getAddress, getAddressById, addAddress, updateAddress, deleteAddress}